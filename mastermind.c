#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void user_guess(int *guess);
void secret_number(int *secret);
int guess_check (int *guess, int *secret, int *turn_counter);


int main(int argc, char *argv[])
{
  int turn_counter = 0;
  int secret[4];
  
  // Using argc to determine where secret number comes from
  // argc = 1 no user input and pick random. argc = 2 read from file.mm to
  // get number.
  if (argc == 1){
    secret_number(secret);

  // with 2 cmd line arguments argc[1] is file with target number
  // if any errors in retrieving or reading the target from file
  // will just default to picking a random number.
  }else if (argc == 2){
    // ensuring argument supplied has a .mm file extension
    char test[32];
    strncpy(test, argv[1], 16);
    int length = strlen(test);
    char file[3]={test[length-3], test[length-2], test[length-1]};
    char comp[3] = ".mm";
    // testing the last three char of the file supplied in cmd line to 
    // see if it is a .mm file
    if (file[0] != comp[0] || file[1] != comp[1] || file[2] != comp[2]){
      printf("File supplied is not a .mm file, defaulting to random secret\n");
      secret_number(secret);
      goto play_game;
    }

    // opening file, if unable defaulting to random secret number
    FILE *fp = fopen(argv[1], "r");
    if (!fp){
      printf("Unable to open file 1 for reading, defualting to random secret.\n");
      secret_number(secret);
      goto play_game;
    }

    char buf[128];
    fgets(buf, sizeof(buf), fp);
    // If string is to long default to random secret
    if (strlen(buf) > 5){
      printf("File target is to large, defaulting to random secret.\n");
      secret_number(secret);
    //  Made it this far you can use the target in the file as long as it is all digits
    }else{
      strtol(buf,NULL,10);
      for(int i = 0; i < 4 ; ++i){
        // ensure all parts of target number are digits and if not default to random
        if (isdigit(buf[i])){
          secret[i] = buf[i] - '0';
          // string conversion to int taken from and storage into secret array
          // stackoverflow.com/questions/19468556/converting-char-array-to-int-arry
          // '0' subtracts ASCII value from the char leaving only the integers 0-9.
          // isdigit and digit monitoring ensure only numbers 0-9 will make it here.
        }else{
          printf("Target value contains a non-numeric value. Defaulting to random secret.\n");
          secret_number(secret);
        }
      }
    }
    fclose(fp);

  // If more than 1 other cmd line argument just default to random secret
  }else{
      fprintf(stderr, "Usage: %s <file1> <file2>\n", argv[0]);
      printf("Defaulting to random secret.\n");
      secret_number(secret);
  }

play_game:
// Enter the guessing loop. All who enter do not leave until the 
// secret number has been found.
  while(1){
    int guess[4];
    // Jump to function to gather users guess
    user_guess(guess);

    // Test users guess returns 1 when win, all other times returns 0
    int test = guess_check(guess, secret, &turn_counter);
    // If test is 1 you won and break out of guessing loop
    if (test){
      break;
    }
  }
  printf("You win! It took you %d guessess\n", turn_counter);
}


// Function for obtainig the users guess
void user_guess(int *guess)
{
  char temp[128];

guess:

  // Capture users 4 digit guess
  printf("Guess a four digit number: ");
  scanf("%s", temp);

  // Error handle for an input longer than 4 characters
  if (isprint(temp[4])){
    printf("You enteered a number with more than four characters,\n");
    goto guess;
  }

  // Users number is put into an array
  for(int i = 0; i < 4 ; ++i){
    if (!isdigit(temp[i])){
      printf("You entered a Non-Digit or under 4 digits.\n");
      goto guess;
    }
    // string conversion to int taken from
    // stackoverflow.com/questions/19468556/converting-char-array-to-int-arry
    // '0' subtracts ASCII value from the char leaving only the integers 0-9.
    // isdigit and digit monitoring ensure only numbers 0-9 will make it here.
    guess[i] = temp[i]- '0';
  }
}


// Function to check the users guess to the secret number
int guess_check (int *guess, int *secret,int *turn_counter)
{
  // int check_guess[4] = {1,1,1,1} & int check_guess[4] = {1,1,1,1}
  // used to keep track of what digits in the secret have beeen matched with
  // users guess for each itteration of checking. When matched the corresponding
  // index in the arrays get turned to 0 so they canbe skipped over in the following
  // iterations of this guess. Both arrays are reset at the start of each guess check
  int check_secret[4] = {1,1,1,1};
  int check_guess[4] = {1,1,1,1};
  int red_count = 0;
  int white_count = 0;

  *turn_counter += 1;

  // checking for the reds
  for (int i = 0; i < 4; ++i){
    if (guess[i] == secret[i]){
      ++red_count;
      //  if they are equal they don't need to be compared in the white
      check_secret[i] = check_guess[i] =0;
      // match all 4 you win!!!
      if (red_count == 4){
        return 1;
      }
    }
  }
  // When no red matches, omit red print statement
  if (red_count > 0){
    printf("%d Red",red_count);
  }

  // checking for the whites
  for(int i = 0; i < 4; ++i){
    for(int j = 0; j < 4; ++j){
      if (guess[i] == secret[j] && check_secret[j] && check_guess[i] && i != j){
        ++white_count;
        // Blocks digit in secret being double counted for white
        check_secret[j] = check_guess[i] = 0;
        break;
      }
    }
  }

  // Print comma only when printing both white and red results
  if (red_count > 0 && white_count > 0){
    printf(", ");
  }
  if (white_count > 0){
    printf("%d White",white_count);
  }
  if (white_count == 0 && red_count == 0){
    printf("No Matches");
  }
  printf("\n");
  return 0;
}


// Obtaining a secret random number when the user does not provide a target
// from a file.
void secret_number(int *secret)
{
  // seeding RNG with clock time
  srand(time(NULL));

  // Creating a random secret number and storing each digit in the array
  for(int i = 0; i<4 ; ++i){
    secret[i] = rand()%10;
  }
}
